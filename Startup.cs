using Microsoft.AspNetCore.Components.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace WebApplication1
{
    public class Startup
    {

        public void Configure(IComponentsApplicationBuilder app)
        {
            // This is run in the browser's console
            Console.WriteLine("Now in Startup.Configure");

            // This adds the component Index.razor inside the "app" tags of
            // wwwroot/index.html
            app.AddComponent<Index>("app");
        }
    }
}
