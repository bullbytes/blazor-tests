# What is this?
A minimal example of using [Blazor](https://github.com/aspnet/Blazor) in the browser.

# Build instructions

    dotnet run

This should start the server serving the website at https://localhost:5001/

# Dependencies
[`dotnet`](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet) version 3.0.100.

---

If you're on Arch Linux, you can use [dotnet-install.sh](https://wiki.archlinux.org/index.php/.NET_Core) to get the right `dotnet` version.

