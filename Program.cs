﻿using Microsoft.AspNetCore.Blazor.Hosting;
using System;

namespace WebApplication1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // This is run in the browser's console
            Console.WriteLine("Now in Program.Main");
            CreateHostBuilder(args).Build().Run();
        }

        public static IWebAssemblyHostBuilder CreateHostBuilder(string[] args) {
            // This will cause method Configure in Startup.cs to be called
            return BlazorWebAssemblyHost.CreateDefaultBuilder()
                .UseBlazorStartup<Startup>();
        }
    }
}
